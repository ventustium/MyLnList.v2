<?php

namespace App\Models\LightNovel;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Volume extends Model
{
    use HasFactory;
    protected $table = "light_novel.volumes";
    protected $guarded = [];

    public function chapter()
    {
        return $this->hasMany(Chapter::class);
    }

    public function series()
    {
        return $this->belongsTo(Series::class);
    }
}
