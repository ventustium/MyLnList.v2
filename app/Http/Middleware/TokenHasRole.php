<?php

namespace App\Http\Middleware;

use Closure;
use App\ResponseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class TokenHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     * @param  string  $role
     * @return mixed
     */

    use ResponseController;

    public function handle(Request $request, Closure $next, ...$role): Response
    {
        $user = Auth::guard('sanctum')->user();

        if (!$user || !$user->hasRole($role)) {
            return $this->unauthorized(Route::currentRouteName());
        }

        return $next($request);
    }
}
