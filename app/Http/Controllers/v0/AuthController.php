<?php

namespace App\Http\Controllers\v0;

use App\Models\User;
use App\ResponseController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use ResponseController;

    public function login(Request $request){
        if (empty(request('email')) && empty(request('username'))) {
            return $this->notFilledCorrect("login");
        }

        $validation = Validator::make($request->all(),[
            'password' => 'required'
        ]);

        if($validation->fails()){
            return $this->notFilledCorrect("login");
        }
        else{
            $password = sha1($request->password);
            $user = User::where('username', $request->username)->where('password', $password)->first();

            if ($user) {
                Auth::login($user);
                $token = $user->createToken("API TOKEN")->plainTextToken;
                $response = [
                    'request' => 'login',
                    'status' => 302,
                    'message' => "Login Success. DO NOT SHARE YOUR TOKEN!",
                    'token' => $token
                ];
                return response()->pJson($response, 302)->header('Authorization', 'Bearer '.$token);
            }

            $user = User::where('email', $request->email)->where('password', $password)->first();
            if ($user) {
                Auth::login($user);
                $token = $user->createToken("API TOKEN")->plainTextToken;
                $response = [
                    'request' => 'login',
                    'status' => 302,
                    'message' => "Login Success. DO NOT SHARE YOUR TOKEN!",
                    'token' => $token
                ];
                return response()->pJson($response, 302)->header('Authorization', 'Bearer '.$token);
            }

            if(Auth::attempt($request->only(['email', 'password']))){
                $user = User::where('email', $request->email)->first();
                $token = $user->createToken("API TOKEN")->plainTextToken;
                $response = [
                    'request' => 'login',
                    'status' => 302,
                    'message' => "Login Success. DO NOT SHARE YOUR TOKEN!",
                    'token' => $token
                ];
                return response()->pJson($response, 302)->header('Authorization', 'Bearer '.$token);
            }

            if(Auth::attempt($request->only(['username', 'password']))){
                $user = User::where('username', $request->username)->first();
                $token = $user->createToken("API TOKEN")->plainTextToken;
                $response = [
                    'request' => 'login',
                    'status' => 302,
                    'message' => "Login Success. DO NOT SHARE YOUR TOKEN!",
                    'token' => $token
                ];
                return response()->pJson($response, 302)->header('Authorization', 'Bearer '.$token);
            }

            else{
                $response = [
                    'request' => 'login',
                    'status' => 401,
                    'message' => "Login Failed"

                ];
                return response()->pJson($response, 401);
            }
        }
    }

    public function logout(Request $request){
            $user = auth('sanctum')->user();
            $user->currentAccessToken()->delete();
            $response = [
                'request' => 'logout',
                'status' => 200,
                'message' => "Logout Success"
            ];
            return response()->pJson($response, 200);
    }

    public function logoutAll(Request $request){
            $user = auth('sanctum')->user();
            $user->tokens()->delete();
            $response = [
                'request' => 'logoutAll',
                'status' => 200,
                'message' => "Logout Success"
            ];
            return response()->pJson($response, 200);
        }

    public function checkToken(){
        if(auth('sanctum')->check()){
            $response = [
                'request' => 'Check Token',
                'status' => 200,
                'message' => "Token Found",
                'data' => auth('sanctum')->user()
            ];
            return response()->pJson($response, 200);
        }
    }
}
